# Quellenangabe

http://www.gwtproject.org/doc/latest/tutorial/gettingstarted.html

Funktion: StockWatcher

# Aufbau Projekt

webAppCreator -out C:\DataSource\Java\GWT_Test_2 -templates sample,maven,eclipse com.SWSoft.GWT_Test_2

# Ausf�hrung

(mvn gwt:run)
mvn gwt:devmode

# GWT Doku

## Widget Gallery
http://www.gwtproject.org/doc/latest/RefWidgetGallery.html

# Build Prozess

* Speichern der Quellen
* Im Projektverzeichnis "mvn install"
* In Eclipse Projekt -> Run As (Debug As) -> GWT Development Mode with Jetty -> Browser -> http://127.0.0.1:8888/GWT_Test_2.html

Alternativ �ber die Console:
* mvn install gwt:devmode

# Versionshistorie

## 1.1.0
Implementierung der serverseitig ermittelten Kurse
Demonstration von Exceptions �ber die RPC Schnittstelle

## 1.0.0
Rein clientseitige, lauff�hige Version des Stockwatchers